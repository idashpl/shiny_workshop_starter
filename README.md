
## Installation

Required packages might be installed by executing the code below:

```r
install.packages("shiny")
install.packages("nycflights13")
install.packages("dplyr")
install.packages("ggplot2")
install.packages("ggthemes")
```

You should also have R (at least 3.3.X).
